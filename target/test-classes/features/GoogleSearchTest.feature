@googleSearchTest
Feature: Google Homepage Search

  Scenario: User can search with "Google Search"
    Given The user is on Google home page
    When  The user performs the searching "The name of the wind"
    And   The user clicks on the first link on Google results page
    Then  The user should be into "patrickrothfuss" books page


  Scenario: User can search by using the suggestions
    Given The user is on Google home page
    When  The user clicks on the first Google suggestion after typing "The name of the w"
    And   The user clicks on the first link on Google results page
    Then  The user should be into "patrickrothfuss" books page

