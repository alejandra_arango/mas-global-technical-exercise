package locators;

public class Locators {

    //Search Section
    public final static String searchTextbox = "input[name = 'q']";
    public final static String googleSearchButton = "input[value= 'Buscar con Google']";
    public final static String firstGoogleSuggestion = "li[data-view-type = '1']:first-child";

    //Google Results Page
    public final static String googleFirstResult = "(//div[@class ='r'])[1]";

}
