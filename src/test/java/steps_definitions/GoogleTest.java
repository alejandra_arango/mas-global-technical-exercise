package steps_definitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import data_pool.Data;
import org.openqa.selenium.WebDriver;
import pages.GoogleResultsPage;
import pages.SearchSection;
import support.AutomationWebDriver;


import static junit.framework.TestCase.assertTrue;


public final class GoogleTest extends AutomationWebDriver {

    public WebDriver driver;

    @Before("@googleSearchTest")
    public void setUp() {
        driver = getDriver();
    }

    @After("@googleSearchTest")
    public void tearDown() {
        clear_status();
    }

    @Given("^The user is on Google home page")
    public void an_user_logged_into_confluence_space() {
        openURL(Data.url);
    }

    @When("^The user performs the searching \"([^\"]*)\"$")
    public void theUserTypesIntoTheSearchField(String searchText){
        SearchSection searchSection = new SearchSection(driver);
        searchSection.typeGoogleSearch(searchText);
        searchSection.clickOnGoogleSearchButton();
    }

    @And("^The user clicks on the first link on Google results page$")
    public void theUserClicksOnTheFirstLinkOnGoogleResultsPage() {
        GoogleResultsPage googleResultsPage = new GoogleResultsPage(driver);
        googleResultsPage.clickOnFirstGoogleResult();
    }

    @Then("^The user should be into \"([^\"]*)\" books page")
    public void theUserShouldBeIntoThePagePage(String expectedURL) {
        assertTrue("The URL should contains " + expectedURL + " but was " + driver.getCurrentUrl() ,driver.getCurrentUrl().contains(expectedURL));
    }

    @When("^The user clicks on the first Google suggestion after typing \"([^\"]*)\"$")
    public void theUserClicksOnTheFirstSuggestionOfGoogleAfterTyping(String searchText) {
        SearchSection searchSection = new SearchSection(driver);
        searchSection.typeGoogleSearch(searchText);
        searchSection.clickOnFirstGoogleSuggestion();
    }
}
