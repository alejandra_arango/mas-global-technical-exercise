package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GoogleTestPage {

    WebDriver driver;
    WebDriverWait wait;

    /**
     * Constructor
     *
     * @param driver Web driver
     */
    public GoogleTestPage(WebDriver driver) {
        this.driver = driver;
        //Page factory
        PageFactory.initElements(this.driver, this);
        //Web Driver Wait
        wait = new WebDriverWait(this.driver, 30);

    }

    /**
     * Clicks on a given web element
     *
     * @param element Web Element to be clackable
     */
    public void clickOnElement(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    /**
     * Types a given text on a Web Element
     *
     * @param element Element to be typed
     * @param text    Text to be set
     */
    public void typesIntoElement(WebElement element, String text) {
        wait.until(ExpectedConditions.visibilityOf(element));
        element.sendKeys(text);
    }

}
