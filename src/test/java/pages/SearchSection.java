package pages;

import locators.Locators;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchSection extends GoogleTestPage {
    /**
     * Constructor
     *
     * @param driver Web driver
     */
    public SearchSection(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = Locators.searchTextbox)
    private WebElement searchTextbox;

    @FindBy(css = Locators.googleSearchButton)
    private WebElement googleSearchButton;

    @FindBy(css = Locators.firstGoogleSuggestion)
    private WebElement firstGoogleSuggestion;

    /**
     * Types a given text into Google Search Textbox
     * @param searchText text to be searched
     */
    public void typeGoogleSearch (String searchText){
        typesIntoElement(searchTextbox, searchText);
    }

    /**
     * Clicks on "Google Search Button"
     */
    public void clickOnGoogleSearchButton(){
        clickOnElement(googleSearchButton);
    }

    /**
     * Clicks on "Google Search Button"
     */
    public void clickOnFirstGoogleSuggestion(){
        clickOnElement(firstGoogleSuggestion);
    }
}
