package pages;

import locators.Locators;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GoogleResultsPage extends GoogleTestPage {
    /**
     * Constructor
     *
     * @param driver Web driver
     */
    public GoogleResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = Locators.googleFirstResult)
    private WebElement googleFirstResult;


    /**
     * Clicks on the first result on "Google" search
     */
    public void clickOnFirstGoogleResult (){
        clickOnElement(googleFirstResult);
    }

}
