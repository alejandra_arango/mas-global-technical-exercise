# MAS Global Hiring - Technical Exercise

![](QEHiringMG.gif)

## Table of Contents

- [1. Challenge](#1-challenge)
  - [1.1. User Stories](#11-user-stories)
  - [1.2. Test Cases](#12-user-stories)
  - [1.3. Description](#13-description)
- [2. Stack](#2-stack)
- [3. Getting Started](#3-getting-started)
  - [3.1. Requirements](#31-requirements)
  - [3.2. Setup environment](#32-setup-environment)
- [4. Folder Structure](#4-folder-structure)
- [5. Notes](#5-notes)
- [6. Author](#author)

## 1. Challenge

### 1.1. User Stories

Feature: Google Homepage Search

**_Scenario:_** User can search with “Google Search”

**Given** I’m on the homepage

**When** I type “The name of the wind” into the search field

**And** I click the Google Search button

**Then** I go to the search results page

**And** the first result is “The Name of the Wind - Patrick Rothfuss”

**When** I click on the first result link

**Then** I go to the “Patrick Rothfuss - The Books” page

**_Scenario:_** User can search by using the suggestions

**Given** I’m on the homepage

**When** I type “The name of the w” into the search field

**And** the suggestions list is displayed

**And** I click on the first suggestion in the list

**Then** I go to the search results page

**And** the first result is “The Name of the Wind - Patrick Rothfuss”

**When** I click on the first result link

**Then** I go to the “Patrick Rothfuss - The Books” page
### 1.2. Test Cases

- Test Case ID: 0001

**Browsers:** Chrome version 72.0.3626.121

**Description:** Verify that the user can search any text in "Google" page

**Test Steps:**

1.  Go to www.google.com
2. Type "The name of the wind” on search textbox
3. Click on search button

**Expected Results:** 
- The results page should contain results related with the searched text (pass)
- The first Result should be “Patrick Rothfuss - The Books” page (Fail)

**Actual Result:** The first result is a wikipedia page

- Test Case ID: 0002

**Browsers:** Chrome version 72.0.3626.121

**Description:** User can search by using the suggestions

**Test Steps:**

1. Go to www.google.com
2. Type "The name of the w” on search textbox
3. Select the first suggestion

**Expected Results:** 
- The results page should contain results related with the searched text (pass)
- The first Result should be “Patrick Rothfuss - The Books” page (Fail)

**Actual Result:** The first result is a wikipedia page


### 1.3. Description

You are testing https://www.google.com.
- You need to create manual test cases according to the stories described previously.
- Then, you need to create an automation framework from scratch that allows you to
automate the test cases created in the previous step. You can use the programming
language you feel more comfortable with.
- Always make use of the OOP (abstraction, polymorphism, encapsulation, and
Inheritance) principles when designing your Solution.
- Implement your automation solution following the Page Object Model pattern.

## 2. Stack

The project was build under above technologies

- Selenium v3.9.1
- Cucumber v1.2.2
- Java v9.0.4
- OS Windows 
- Chrome Driver v73.0.3683.20

## 3. Getting Started

### 3.1. Requirements

- [Maven](https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows)
- Standard Edition Development Kit (JDK™)

### 3.2. Setup environment

1. Check that the system meet the requirements.
2. Clone repository from ´https://alejandra_arango@bitbucket.org/alejandra_arango/mas-global-hiring.git´
3. Run ´mvn -Dtest=CucumberRunner test´


```bash
$ git clone https://alejandra_arango@bitbucket.org/alejandra_arango/mas-global-hiring.git
$ cd qe-hiring-technical-exercise
$ mvn -Dtest=CucumberRunner test
```

## 4. Folder Structure

```
QEHiring
 ┣ data_pool
 |   └── Data.java
 ┣ locators
  |   └── Locators.java
 ┣ pages
  |   └── GoogleResultsPage.java
  |   └── GoogleTestPage.java
  |   └── SearchSection.java
 ┣ steps_definition
  |   └── GoogleTest.java
 ┣ support
  |   └── AutomationWebDriver.java
 ┣ cucumberRunner.java
 ┣ resources
  |   └── features
  |         └── GoogleSearchTest.feature
  |   └── ChromeDriver.exe
 ┣ README.md
```
An overview of what each of these does:

| FILE / DIRECTORY |      Description                                  |
|------------------|-------------------------------------------------|
| data_pool        | Aim of data needed on the project execution       | 
| locators         | Contains a java file with used test locators  | 
| pages            | Pages of Google site based on POM             | 
| steps_definition | Java method with an expression that links it to one or more Gherkin steps. | 
| support          | Automation Web Driver configuration and methods | 
| resources        | Chrome Driver and features based on BDD| 

## 5. Notes

For this project I decided to use BDD (Behaviour-Driven Developmen) in order to have clarity of every scenario that I decided to create for the exercise requirements. 
It is important to mention that the tests are failing because te first result that Google shows is a Wikipedia page not the page described on the exercise.

## Author

[Alejandra Arango Arcila]